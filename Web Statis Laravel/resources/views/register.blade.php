<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="POST">
        <label for="firstname">First name:</label>
        <br><br>
        <input id="firstname" name="firstname" type="text">
        <br><br>
        <label for="lastname">Last name:</label>
        <br><br>
        <input id="lastname" name="lastname" type="text">
        <br><br>
        <input name="gender" id="male" type="radio" value="male">
        <label for="male">Male</label>
        <br>
        <input name="gender" id="female" type="radio" value="female">
        <label for="female">Female</label>
        <br>
        <input name="gender" id="other" type="radio" value="other">
        <label for="other">Other</label>
        <br><br>
        <label for="nationality">Nationality:</label>
        <br><br>
        <select id="nationality" name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="british">British</option>
            <option value="american">American</option>
            <option value="chinese">Chinese</option>
            <option value="indian">Indian</option>
            <option value="other">Other</option>
        </select>
        <p>Language Spoken:</p>
        <input id="bahasa-indonesia" name="bahasa-indonesia" value="bahasa-indonesia" type="checkbox">
        <label for="bahasa-indonesia">Bahasa Indonesia</label>
        <br>
        <input id="english" name="english" value="english" type="checkbox">
        <label for="english">English</label>
        <br>
        <input id="other" name="other" value="other" type="checkbox">
        <label for="other">Other</label>
        <br><br>
        <label for="bio">Bio</label>
        <br><br>
        <textarea id="bio" name="bio" cols="30" rows="10"></textarea>
        <br>
        <input type="submit" value="Sign Up">
        @csrf
    </form>
</body>
</html>