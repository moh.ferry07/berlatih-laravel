@extends("layouts.master")

@section("content")
<div class="card">
    <div class="card-header">
        <h3 class="card-title">Tabel Pemain Film</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        @if(session("success"))
        <div class="alert alert-success">
            {{session("success")}}
        </div>
        @endif
        <a class="btn btn-primary my-3" href="/cast/create">Tambah Pemain Baru</a>
        <table class="table table-bordered">
            <thead>
                <tr class="text-center">
                    <th style="width: 10px">#</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Bio</th>
                    <th style="width: 40px">Action</th>
                </tr>
            </thead>
            <tbody>
                @forelse($cast as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</td>
                    <td>{{$value -> nama}}</td>
                    <td>{{$value -> umur}}</td>
                    <td>{{$value -> bio}}</td>
                    <td class="d-flex">
                        <a href="/cast/{{$value->id}}" class="btn btn-info">Show</a>
                        <a href="/cast/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                        <form action="/cast/{{$value->id}}" method="post">
                            @csrf
                            @method("delete")
                            <input type="submit" class="btn btn-danger" value="Delete">
                        </form>
                    </td>
                </tr>
                @empty
                <tr>
                    <td colspan="5" class="text-center">No Data</td>
                </tr>
                @endforelse
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@endsection