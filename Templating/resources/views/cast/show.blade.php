@extends("layouts.master")

@section("content")
<div class="container mt-3 ml-3">
    <div class="row">
        <div class="col-2">
            <h1>Nama</h1>
        </div>
        <div class="col-10">
            <h1>: {{$cast->nama}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <h1>Umur</h1>
        </div>
        <div class="col-10">
            <h1>: {{$cast->umur}}</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <h1>Bio</h1>
        </div>
        <div class="col-10">
            <h1>: {{$cast->bio}}</h1>
        </div>
    </div>
</div>
@endsection