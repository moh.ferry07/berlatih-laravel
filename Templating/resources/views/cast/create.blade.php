@extends("layouts.master")

@section("content")
<div class="card card-primary ml-3 mt-3">
    <div class="card-header">
        <h3 class="card-title">Tambah Pemain Film</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" method="post" action="/cast">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="nama">nama</label>
                <input type="text" class="form-control" id="nama" placeholder="masukkan nama" name="nama" value="{{old("nama","")}}">
                @error('nama')
                <div class=" alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="umur">umur</label>
                <input type="number" class="form-control" id="umur" placeholder="masukkan umur" name="umur" value="{{old("umur","")}}">
                @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="bio">bio</label>
                <input type="text" class="form-control" id="bio" placeholder="masukkan bio" name="bio" value="{{old("bio","")}}">
                @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
                @enderror
            </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Tambah</button>
        </div>
    </form>
</div>
@endsection